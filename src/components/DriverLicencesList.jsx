import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import DriverLicence from './DriverLicence.jsx';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function BasicGrid(props) {
  return (
    <Box sx={{ flexGrow: 1 }} style={{marginTop: 80}}>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Item>Placeholder: Add New Driver Licence</Item>
        </Grid>
        <Grid item xs={4}>
          <Item>
            <DriverLicence data={props.data}></DriverLicence>
          </Item>
        </Grid>
        <Grid item xs={4}>
          <Item>Placeholder: Expired Driver Licence</Item>
        </Grid>

      </Grid>
    </Box>
  );
}