const config = {
  Auth: {
    region: 'ap-southeast-2',
    userPoolId: 'ap-southeast-2_REPLACE_ME',
    userPoolWebClientId: 'REPLACE_ME',
    oauth: {
      domain: 'https://REPLACE_ME.auth.ap-southeast-2.amazoncognito.com',
      scope: ['email', 'profile', 'openid'],
      // scope: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
      redirectSignIn: window.location.origin,
      redirectSignOut: window.location.origin,
      responseType: 'token'
    }
  },
  API: {
    endpoints: [
      {
        name: "baseApi",
        endpoint: "https://REPLACE_ME.execute-api.ap-southeast-2.amazonaws.com"
      }
    ]
      
  }
};

export default config;